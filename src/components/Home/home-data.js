import codePurple from "../../images/code-purple.jpg";

export const HomeData = {
  id: "home",
  title: "Świetne programy",
  subtitle:
    "Jesteśmy firmą spod Gniezna produkującą nowatorskie oprogramowanie dla administracji i wszystkich gałęzi przemysłu",
  buttonText: "Zobacz nasze produkty",
  img: codePurple,
};
