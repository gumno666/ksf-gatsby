import {
  faCode,
  faChalkboard,
  faUsers,
} from "@fortawesome/free-solid-svg-icons";

export const ServicesData = [
  {
    title: "Web Development",
    img: faCode,
    text: "Oferujemy tworzenie szerokiego wachlarza stron i aplikacji webowych na wszystkie platformy",
  },
  {
    title: "Training",
    img: faChalkboard,
    text: "Szkolenia indywidualne i grupowe z języków Python i JavaScript, online i stacjonarnie",
  },
  {
    title: "Internet Marketing",
    img: faUsers,
    text: "Zajmujemy się prowadzeniem kampanii reklamowych na Facebooku, Instagramie, LinkedIn i innych portalach społecznościowych",
  },
];

export default ServicesData;
