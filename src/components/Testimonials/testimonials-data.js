import customer1 from "../../images/testimonials/customer1.png";
import customer2 from "../../images/testimonials/customer2.png";
import customer3 from "../../images/testimonials/customer3.png";
import customer4 from "../../images/testimonials/customer4.jpeg";

export const TestimonialsData = {
  title: "Nasi klienci o nas: ",
  subtitle: "Cenimy sobie partnerską relację z klientem",
  customerStories: [
    {
      customerName: "Bartosz Jaworski",
      customerImg: customer1,
      companyName: "Bruska i s-ka",
      storyText:
        "Krzysztof i Fabian podjęli udaną próbę przebudowy naszego systemu informatycznego, która przyniosła naszej firmie wielkie korzyści zarówno finansowe, jak i pod względem komfortu pracy. Najwyższej jakości profesjonaliści!",
    },
    {
      customerName: "Paweł Kurmin",
      customerImg: customer2,
      companyName: "Mlekpol",
      storyText:
        "Program do obsługi dostaw wprowadzony przez KSF w rekordowym czasie 2 tygodni pozwolił na niezwykle sprawną optymalizację działań jednego z najistotniejszych obszarów działań naszej firmy.",
    },
    {
      customerName: "Olga Kisiel",
      customerImg: customer3,
      companyName: "NASA",
      storyText:
        "Współpraca z firmą KSF przebiegała w bardzo miłej atmosferze. Programistów firmy cechuje empatia, wysoka kultura i wysokiej klasy profesjonalizm. Najtrudniejsze koncepty istotne dla działania tworzonej dla nas aplikacji potrafili przedstawić w jasny i przejrzysty sposób. Gorąco polecam!",
    },
    {
      customerName: "Marta Wudel",
      customerImg: customer4,
      companyName: "Santander Bank",
      storyText:
        "Programiści z firmy KSF stworzyli niezwykle udany program szkoleń z podstaw języka Python do zastosowań analitycznych dla pracowników kilku oddziałów naszego banku. Planujemy dalszą współpracę w zakresie szkoleń.",
    },
  ],
};
