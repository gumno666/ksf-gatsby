export const NavData = {
  leftPane: { text: "KSF" },
  rightPane: [
    { tabName: "Home" },
    { tabName: "Services" },
    { tabName: "About Us" },
    { tabName: "Portfolio" },
    { tabName: "Testimonials" },
    { tabName: "Contact" },
  ],
};
