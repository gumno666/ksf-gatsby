import kszyda from "../../images/team/krzysztof-szyda.png";
import eliput from "../../images/team/emilia-liput.jpg";
import fbernat from "../../images/team/fabian-bernat.png";

export const TeamData = {
  title: "Nasz zespół",
  teamMembers: [
    {
      name: "Krzysztof Szyda",
      img: kszyda,
      description:
        "Krzysztof to CEO i twórca strategii firmy, pasjonat dobrej kawy i polskiego rocka.",
    },
    {
      name: "Emilia Liput",
      img: eliput,
      description:
        "Z wykształcenia nauczycielka fizyki, w naszej firmie zajęła się administracją i marketingiem, od niedawna jest odpowiedzialna również za SEO.",
    },
    {
      name: "Fabian Bernat",
      img: fbernat,
      description:
        "Pasjonat języka JavaScript, specjalista od przygotowania programu szkoleń. W wolnym czasie skate i ojciec dwuletniej Angeliki.",
    },
  ],
};
